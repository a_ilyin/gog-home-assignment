<?php


namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ProductControllerTest
 *
 * @package Tests\AppBundle\Controller
 */
class ProductControllerTest extends WebTestCase
{

    /**
     * Test for paginatedProductsListAction()
     */
    public function testPaginatedProductsListAction()
    {
        $client = static::createClient();

        $client->request('GET', '/api/products/1');

        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', '/api/products/2');

        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * Test for showSingleProductAction()
     */
    public function testShowSingleProductAction()
    {
        $client = static::createClient();

        $client->request('GET', '/api/products/single/1');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
          '{"id":1,"title":"Fallout","price":1.99}',
          $client->getResponse()->getContent());

        $client->request('GET', '/api/products/single/4');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
          '{"id":4,"title":"Don`t starve","price":4.99}',
          $client->getResponse()->getContent());

        $client->request('GET', '/api/products/single/6');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString(
          '{"id":6,"title":"Doot","price":6.99}',
          $client->getResponse()->getContent());
    }

    /**
     * Test for postProductAction()
     */
    public function testPostProductAction()
    {
        $client = static::createClient();

        $client->request('POST', '/api/products/add', ['title' => 'Witcher 2', 'price' => 24.13]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * Test for updateAction()
     */
    public function testUpdateAction()
    {
        $client = static::createClient();

        $client->request('PUT', '/api/products/7/edit', ['title' => 'Witcher', 'price' => 10.56]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('PUT', '/api/products/1/edit', ['title' => 'Fallout 2']);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('PUT', '/api/products/2/edit', ['price' => 3.40]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * Test for deleteAction()
     */
    public function testDeleteAction()
    {
        $client = static::createClient();

        $client->request('DELETE', '/api/products/5/delete');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
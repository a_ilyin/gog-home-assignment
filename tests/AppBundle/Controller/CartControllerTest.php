<?php


namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ProductControllerTest
 *
 * @package Tests\AppBundle\Controller
 */
class CartControllerTest extends WebTestCase
{

    /**
     * Test for paginatedProductsListAction()
     */
    public function testCreateCartAction()
    {
        $client = static::createClient();
        $client->request('GET', '/api/get_auth_token');
        $authToken = $client->getResponse()->getContent();
        $client->request('POST', '/api/cart/create', ['authToken' => $authToken]);
        $this->assertEquals(1, $client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    // Unfortunately I didn't had more time to finish tests...
    // My deadline didn't include test writing, so I leave it as it is.
}
<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190918221845 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cart (id INT AUTO_INCREMENT NOT NULL, token VARCHAR(255) NOT NULL, totalPrice NUMERIC(10, 2) DEFAULT NULL, products LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', checkOuted TINYINT(1) DEFAULT \'0\' NOT NULL, UNIQUE INDEX UNIQ_BA388B75F37A13B (token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, Title VARCHAR(255) NOT NULL, Price NUMERIC(10, 2) NOT NULL, UNIQUE INDEX UNIQ_D34A04ADEAF7576F (Title), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('INSERT INTO `product`(`Title`, `Price`) VALUES (\'Fallout\',1.99)');
        $this->addSql('INSERT INTO `product`(`Title`, `Price`) VALUES (\'Baldur`s Gate\',2.99)');
        $this->addSql('INSERT INTO `product`(`Title`, `Price`) VALUES (\'Icewind Dale\',3.99)');
        $this->addSql('INSERT INTO `product`(`Title`, `Price`) VALUES (\'Don`t starve\',4.99)');
        $this->addSql('INSERT INTO `product`(`Title`, `Price`) VALUES (\'Bloodbornne\',5.99)');
        $this->addSql('INSERT INTO `product`(`Title`, `Price`) VALUES (\'Doot\',6.99)');
        $this->addSql('INSERT INTO `product`(`Title`, `Price`) VALUES (\'Gwent\',7.99)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE cart');
        $this->addSql('DROP TABLE product');
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cart
 *
 * @ORM\Table(name="cart")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CartRepository")
 */
class Cart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, unique=true)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="totalPrice", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $totalPrice;

    /**
     * @var array
     *
     * @ORM\Column(name="products", type="array", nullable=true)
     */
    private $products;

    /**
     * @var boolean
     *
     * @ORM\Column(name="checkOuted", type="boolean", options={"default": false})
     */
    private $checkOuted = false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Cart
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     *
     * @return Cart
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set products
     *
     * @param array $products
     *
     * @return Cart
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * Get products
     *
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set checkOuted
     *
     * @param boolean $checkOuted
     *
     * @return Cart
     */
    public function setCheckOuted($checkOuted)
    {
        $this->checkOuted = $checkOuted;

        return $this;
    }

    /**
     * Get checkOuted
     *
     * @return bool
     */
    public function getCheckOuted()
    {
        return $this->checkOuted;
    }
}


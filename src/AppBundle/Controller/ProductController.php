<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Product;

/**
 * Class ProductController
 *
 * @package AppBundle\Controller
 */
class ProductController extends AbstractFOSRestController
{
    /**
     * Product not found message
     */
    const PRODUCT_NOT_FOUND_MESSAGE = 'Product not found';

    /**
     * Gets list of products
     *
     * Gets list of products with 3 products per page. This number could be chaged.
     * @see \AppBundle\Repository\ProductRepository::getProductsPerPage()
     *
     * @Rest\Get("/api/products/{page}")
     *
     * @param int $page
     *   Number of page to show
     *
     * @return array|View
     *   Returned array mapping:
     *   [
     *     'products' => array of products,
     *     'productsTotalCount' => total products count,
     *     'productsCount' => products count on current page,
     *     'pagesTotal' => total number of pages,
     *     'pageCurrent' => curren page number,
     *   ]
     */
    public function paginatedProductsListAction($page = 1)
    {
        if (!$page || $page < 0 || !is_numeric($page)) {
            return new View(
              'What a curious person! Unfortunately no such page ever existed :(',
              Response::HTTP_NOT_FOUND
            );
        }
        $productsRepository = $this->getDoctrine()
          ->getRepository("AppBundle:Product");
        $products = $productsRepository->getPaginatedProducts($page);

        $productsPerPage = $productsRepository->getProductsPerPage();
        $productsTotal = $products->count();
        $productsOnPage = $products->getIterator()->count();

        $pagesTotal = (int) ceil($productsTotal / $productsPerPage);

        if (!$productsTotal) {
            return new View(
              'There are no products',
              Response::HTTP_NOT_FOUND
            );
        }

        if (!$productsOnPage || ($pagesTotal < $page)) {
            return new View(
              'There are no products on this page... Yet...',
              Response::HTTP_NOT_FOUND
            );
        }

        return [
            'products' => $products->getIterator(),
            'productsTotalCount' => $productsTotal,
            'productsCount' => $productsOnPage,
            'pagesTotal' => $pagesTotal,
            'pageCurrent' => $page,
        ];
    }

    /**
     * Show single product
     *
     * @Rest\Get("/api/products/single/{productId}")
     *
     * @param int $productId
     *
     * @return Product|View
     */
    public function showSingleProductAction($productId)
    {
        return $this->getProductHelper($productId);
    }

    /**
     * Post product action
     *
     * @Rest\Post("/api/products/add")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postProductAction(Request $request)
    {
        $title = $request->get('title');
        $price = $request->get('price');

        if (empty($title) || empty($price)) {
            return new View(
              "Null values are not allowed",
              Response::HTTP_NOT_ACCEPTABLE
            );
        }

        $productRepository = $this->getDoctrine()
          ->getRepository('AppBundle:Product');
        $existingProduct = $productRepository->findBy(['title' =>$title]);
        if ($existingProduct) {
            return new View(
              "Product with title " . $title . " already exists",
              Response::HTTP_NOT_ACCEPTABLE
            );
        }

        $product = new Product();
        $product->setTitle($title);
        $product->setPrice($price);

        $this->flushToEntityManager($product);

        return new View(
          "Product Added Successfully",
          Response::HTTP_OK
        );
    }

    /**
     * Update Product action
     *
     * @Rest\Put("/api/products/{productId}/edit")
     *
     * @param int $productId
     * @param Request $request
     *
     * @return View
     */
    public function updateAction($productId, Request $request)
    {
        $product = $this->getProductHelper($productId);
        if ($product instanceof View) {
            return $product;
        }

        $title = $request->get('title');
        $price = $request->get('price');

        if (empty($title) && empty($price)) {
            return new View(
              "Title and price cannot be null",
              Response::HTTP_NOT_ACCEPTABLE);
        }

        if (!empty($title)) {
            $product->setTitle($title);
        }

        if (!empty($price)) {
            $product->setPrice($price);
        }

        $this->flushToEntityManager();

        return new View(
          "Product " . $product->getTitle() . " updated successfully",
          Response::HTTP_OK
        );
    }

    /**
     * Delete Product action
     *
     * @Rest\Delete("/api/products/{productId}/delete")
     *
     * @param int $productId
     *
     * @return View
     */
    public function deleteAction($productId)
    {
        $product = $this->getProductHelper($productId);
        if ($product instanceof View) {
           return $product;
        }

        $this->flushToEntityManager($product, true);

        return new View(
          "Successfully deleted!",
          Response::HTTP_OK
        );
    }

    /**
     * Helper method to flush entity to entity manager
     *
     * @param bool|Product $product
     *   False by default. if instance of Product provided new entity will be created
     * @param bool $remove
     *   Flag to show to remove entity or not
     */
    public function flushToEntityManager($product = false, $remove = false) {
        $entityManager = $this->getDoctrine()->getManager();
        if ($product && !$remove) {
            $entityManager->persist($product);
        }
        elseif ($product && $remove){
            $entityManager->remove($product);
        }
        $entityManager->flush();
    }

    /**
     * Helper method to check if product exists.
     *
     * @param int $productId
     *
     * @return View|Product
     */
    public function getProductHelper($productId) {
        $productRepository = $this->getDoctrine()
          ->getRepository('AppBundle:Product');
        $product = $productRepository->find($productId);

        if (empty($product)) {
            return new View(
              self::PRODUCT_NOT_FOUND_MESSAGE,
              Response::HTTP_NOT_FOUND
            );
        }

        return $product;
    }
}

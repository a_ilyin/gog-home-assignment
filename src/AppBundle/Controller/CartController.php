<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cart;
use AppBundle\Entity\Product;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

/**
 * Class CartController
 *
 * @package AppBundle\Controller
 */
class CartController extends AbstractFOSRestController
{
    /**
     * Maximum of different products in a cart
     */
    const MAX_CART_ITEMS = 3;

    /**
     * Maximum of the same products in a cart
     */
    const MAX_PRODUCT_ITEMS = 10;

    /**
     * Get api token
     *
     * Let's consider we use third-party lib or service to get authToken
     *
     * @Rest\Get("/api/get_auth_token")
     *
     * @return string
     *   Auth token string.
     */
    public function getAuthToken()
    {
        $str = rand();
        return md5($str);
    }

    /**
     * Create shopping cart
     *
     * @Rest\Post("/api/cart/create")
     *
     * @param Request $request
     *
     * @return int|View
     *   Cart id or new View
     */
    public function createCartAction(Request $request)
    {
        $authToken = $request->get('authToken');
        $cart = $this->getDoctrine()->getRepository('AppBundle:Cart')
          ->findOneBy(['token' => $authToken]);

        if (!empty($cart)) {
            if ($cart->getCheckOuted()) {
                return new View(
                  "You already have shopping cart, but looks like it's check outed. Please remove it and try again",
                  Response::HTTP_FORBIDDEN
                );
            }

            return $cart->getId();
        }

        $cart = new Cart();
        $cart->setToken($authToken);
        $cart->setTotalPrice(0);

        $this->flushToEntityManager($cart);

        return $cart->getId();
    }

    /**
     * Add product
     *
     * @Rest\Put("/api/cart/{cartId}/{productId}/add")
     *
     * @param int $cartId
     * @param int $productId
     * @param Request $request
     *
     * @return View
     */
    public function addProductAction($cartId, $productId, Request $request)
    {
        $authToken = $request->get('authToken');
        $cart = $this->getDoctrine()->getRepository('AppBundle:Cart')
          ->find($cartId);

        $messages = $this->checkCartTokenCheckOuted($cart, $authToken);
        if (!empty($messages)) {
            return new View(
              implode(' ', $messages),
              Response::HTTP_BAD_REQUEST
            );
        }

        $product = $this->getDoctrine()->getRepository('AppBundle:Product')
          ->find($productId);
        if (empty($product)) {
            return new View(
              "Product not found",
              Response::HTTP_NOT_FOUND
            );
        }

        $productPrice = $product->getPrice();

        $cartProducts = $cart->getProducts();
        if (empty($cartProducts)) {
            $cartProducts = [];
        }

        if (count($cartProducts) == self::MAX_CART_ITEMS
          && !array_key_exists($productId, $cartProducts)) {
            return new View(
              "Product cannot be added. There may be only 3 different products",
              Response::HTTP_OK
            );
        }

        if (array_key_exists($productId, $cartProducts)) {

            $productsQuantity = $cartProducts[$productId]['quantity'];
            if ($productsQuantity < self::MAX_PRODUCT_ITEMS) {
                $cartProducts[$productId]['quantity']++;
                $cartProducts[$productId]['productPrice'] += $productPrice;
            }
            else {
                return new View(
                  "Product cannot be added. There may be only 10 products of the same type",
                  Response::HTTP_OK
                );
            }
        }
        else {
            $cartProducts[$productId] = [
              'product' => $product,
              'quantity' => 1,
              'productPrice' => $productPrice,
            ];
        }

        $cart->setProducts($cartProducts);
        $cart->setTotalPrice($cart->getTotalPrice() + $productPrice);

        $this->flushToEntityManager();

        return new View(
          "Product was added to cart.",
          Response::HTTP_OK
        );
    }

    /**
     * Remove single product item
     *
     * @Rest\Put("/api/cart/{cartId}/{productId}/remove-single")
     *
     * @param int $cartId
     * @param int $productId
     * @param Request $request
     *
     * @return View
     */
    public function removeSingleProductAction($cartId, $productId, Request $request)
    {
        $authToken = $request->get('authToken');
        $cart = $this->getDoctrine()
          ->getRepository('AppBundle:Cart')->find($cartId);

        $messages = $this->checkCartTokenCheckOuted($cart, $authToken);
        if (!empty($messages)) {
            return new View(
              implode(' ', $messages),
              Response::HTTP_BAD_REQUEST);
        }

        $cartProducts = $cart->getProducts();

        $product = $this->getDoctrine()->getRepository('AppBundle:Product')
          ->find($productId);
        if (empty($product) && !array_key_exists($productId, $cartProducts)) {
            return new View(
              "Product not found",
              Response::HTTP_NOT_FOUND
            );
        }

        if ($cartProducts[$productId]['quantity'] < 2) {
            unset($cartProducts[$productId]);
        }
        else {
            $cartProducts[$productId]['quantity']--;
            $cartProducts[$productId]['productPrice'] -= $product->getPrice();
        }

        $cart->setProducts($cartProducts);
        $cart->setTotalPrice(
          $cart->getTotalPrice() - $product->getPrice()
        );

        $this->flushToEntityManager();

        return new View(
          "1 item of " . $product->getTitle() . " was removed from cart.",
          Response::HTTP_OK
        );
    }

    /**
     * Remove all products of a type
     *
     * @Rest\Put("/api/cart/{cartId}/{productId}/remove-all")
     *
     * @param int $cartId
     * @param int $productId
     * @param Request $request
     *
     * @return View
     */
    public function removeProductAction($cartId, $productId, Request $request)
    {
        $authToken = $request->get('authToken');
        $cart = $this->getDoctrine()
          ->getRepository('AppBundle:Cart')->find($cartId);

        $messages = $this->checkCartTokenCheckOuted($cart, $authToken);
        if (!empty($messages)) {
            return new View(
              implode(' ', $messages),
              Response::HTTP_BAD_REQUEST
            );
        }

        $cartProducts = $cart->getProducts();

        $product = $this->getDoctrine()->getRepository('AppBundle:Product')
          ->find($productId);
        if (empty($product) && !array_key_exists($productId, $cartProducts)) {
            return new View(
              "Product not found",
              Response::HTTP_NOT_FOUND
            );
        }

        $productPrice = $cartProducts[$productId]['productPrice'];
        unset($cartProducts[$productId]);

        $cart->setProducts($cartProducts);
        $cart->setTotalPrice($cart->getTotalPrice() - $productPrice);

        $this->flushToEntityManager();

        return new View(
          "Product " . $product->getTitle() . " was removed from cart.",
          Response::HTTP_OK
        );
    }

    /**
     * Clear cart
     *
     * @Rest\Put("/api/cart/{cartId}/clear")
     *
     * @param int $cartId
     * @param Request $request
     *
     * @return View
     */
    public function clearCartAction($cartId, Request $request)
    {
        $authToken = $request->get('authToken');
        $cart = $this->getDoctrine()->getRepository('AppBundle:Cart')
          ->find($cartId);

        $messages = $this->checkCartTokenCheckOuted($cart, $authToken);
        if (!empty($messages)) {
            return new View(
              implode(' ', $messages),
              Response::HTTP_BAD_REQUEST
            );
        }

        if (empty($cart->getProducts()) && $cart->getTotalPrice() == 0.00) {
            return new View(
              "Cart already empty",
              Response::HTTP_OK
            );
        }

        $cart->setProducts([]);
        $cart->setTotalPrice(0);

        $this->flushToEntityManager();

        return new View(
          "Your cart successfully cleared! Let's add products!",
          Response::HTTP_OK
        );
    }

    /**
     * Remove cart
     *
     * @Rest\Delete("/api/cart/{cartId}/remove")
     *
     * @param int $cartId
     * @param Request $request
     *
     * @return View
     */
    public function removeCartAction($cartId, Request $request)
    {
        $authToken = $request->get('authToken');
        $cart = $this->getDoctrine()->getRepository('AppBundle:Cart')
          ->find($cartId);
        if (empty($cart)) {
            return new View(
              "Cart not found",
              Response::HTTP_NOT_FOUND
            );
        }
        else {
            if ($cart->getToken() != $authToken) {
                return new View(
                  "No dirty business, please. We know this isn't your cart",
                  Response::HTTP_FORBIDDEN
                );
            }

            $this->flushToEntityManager($cart, true);

            return new View(
              "Your cart successfully deleted! Hope you'll get back to us!",
              Response::HTTP_OK
            );
        }
    }

    /**
     * List products in cart
     *
     * @Rest\Get("/api/cart/{cartId}")
     *
     * @param int $cartId
     * @param Request $request
     *
     * @return View|array
     */
    public function listProductsAction($cartId, Request $request)
    {
        $authToken = $request->get('authToken');
        $cart = $this->getDoctrine()->getRepository('AppBundle:Cart')
          ->find($cartId);

        $messages = $this->checkCartTokenCheckOuted($cart, $authToken);
        if (!empty($messages)) {
            return new View(
              implode(' ', $messages),
              Response::HTTP_BAD_REQUEST
            );
        }

        $products = $cart->getProducts();
        if (empty($products)) {
            return new View(
              "Your cart is empty.",
              Response::HTTP_OK
            );
        }
        return [
            'products' => $products,
            'totalPrice' => $cart->getTotalPrice(),
        ];
    }

    /**
     * Checkout
     *
     * @Rest\Post("/api/cart/{cartId}/checkout")
     *
     * @param int $cartId
     * @param Request $request
     *
     * @return View
     */
    public function checkoutAction($cartId, Request $request)
    {
        $authToken = $request->get('authToken');
        $cart = $this->getDoctrine()->getRepository('AppBundle:Cart')
          ->find($cartId);

        $messages = $this->checkCartTokenCheckOuted($cart, $authToken);
        if (!empty($messages)) {
            return new View(
              implode(' ', $messages),
              Response::HTTP_BAD_REQUEST
            );
        }

        if (empty($cart->getProducts())) {
            return new View(
              "Your cart is empty",
              Response::HTTP_FORBIDDEN
            );
        }

        $cart->setCheckOuted(true);

        $this->flushToEntityManager();

        return new View(
          "Thank you for checking out. Please wait, you will be redirected to payment page one day....",
          Response::HTTP_OK
        );
    }

    /**
     * Helper method to flush entity to entity manager
     *
     * @param bool|Cart $cart
     *   False by default. if instance of Cart passed new entity will be created
     * @param bool $remove
     *   Flag to show to remove entity or not
     */
    public function flushToEntityManager($cart = false, $remove = false) {
        $entityManager = $this->getDoctrine()->getManager();
        if ($cart && !$remove) {
            $entityManager->persist($cart);
        }
        elseif ($cart && $remove){
            $entityManager->remove($cart);
        }
        $entityManager->flush();
    }

    /**
     * Helper method to check most repetitive cases.
     *
     * @param \AppBundle\Entity\Cart $cart
     * @param $authToken
     *
     * @return array
     *   Array of the messages to show.
     */
    public function checkCartTokenCheckOuted(Cart $cart, $authToken) {
        $messages = [];
        if (empty($cart)) {
            $messages[] = "Cart not found.";
        }
        if ($cart->getToken() != $authToken) {
            $messages[] = "No dirty business, please. We know this isn't your cart.";
        }
        if ($cart->getCheckOuted()) {
            $messages[] = "Cart has already been checkouted. Please remove current cart and create a new one.";
        }

        return $messages;
    }
}

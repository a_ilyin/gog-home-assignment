GOG.com Home assignment
================

I decided to do this assignment using Symfony 3.4. To show my possibility and willing to learn. Previously I hadn't had a possibility to do such task using symfony. Only simple application. And I want to thank you for provided possibility. It was interesting to do it.

Main bundles I used are FOS Rest Bundle, Doctrine Migrations Bundle, Symfony PHPUnit bridge.

###database

All the database update will be applied after setting necessary data in app/config/parameters.yml and running `.bin/console doctrine:migrations:migrate`. Products, cart and migration_versions tables will be created. Also table `products` will be populated with testing data.

Also fresh DB dump can be found under `files` folder. It contains all necessary tables and created content for table `products`

###unit tests
Unit tests prepared for `AppBundle\Controller\ProductController` and can be run using `./vendor/bin/simple-phpunit` command.

###Api routing
Api consists of 2 parts as it was described in Home Assignment.
First part:

####1. Products

- `api/products/{page}` - products list with pagination. 3 items per page.
- `api/products/add` - add product. To add product one should provide 2 parameters: *title* and *price*
- `api/products/{productId}` - show product. To show single product provide correct *$productId*
- `api/products/{productId}/delete` - remove product. To remove product provide correct *$productId*
- `api/products/{productId}/edit` - edit product. To edit product provide correct *$productId* and *title* and/or *price* value

####2. Cart

**All cart's requests require `authToken` parameter with the value obtained in `api/get_auth_token`**
- `api/get_auth_token` - url to obtain *authToken*. Let's consider we use third-party library or service to get it.
- `api/cart/create` - creates empty cart. 
- `api/cart/{cartId}` - lists all products in a cart.
- `api/cart/{cartId}/clear` - clears cart. **This action will remove all the products from the cart**
- `api/cart/{cartId}/remove` - removes cart. **This action will remove cart and all the products**
- `api/cart/{cartId}/checkout` - dummy checkout action that sets *checkOuted* parameter of Cart entity to true.(Added to make more complexity, or it was intended to do so)
- `api/cart/{cartId}/{productId}/add` - add product to cart. To add product to cart one should use correct value for `productId`
- `api/cart/{cartId}/{productId}/remove-single` - remove single product item from cart. This action will remove one item of a product with id `productId`. **Example**: *If there is 5 products with productId equal to 3, this action will leave 4 product with productId equal to 3. If there is only one product with such productId it will be totally removed from cart*
- `api/cart/{cartId}/{productId}/remove-all` - remove product item from cart. This action will remove **one product** from cart. **Example**: *if there are 3 products in cart and each product has 5 items(Fallout - 5, Don't starve - 5, Icewind Dale - 5) product with provided productId will be totally removed from cart. Let's say that Don't starve has productId equal to 2 and if one visit url `api/cart/{cartId}/2/remove-all` this action will remove Don't starve from the cart(Fallout - 5, Icewind Dale - 5)*

